<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    //table name
    protected $table = 'posts';

    public $primarykey = 'id';

    //public $timestamps = $true;

}
