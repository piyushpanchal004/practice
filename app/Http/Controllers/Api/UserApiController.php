<?php

namespace App\Http\Controllers\Api;
use App\Http\Resources\UserCollection\UserResourec;
use App\Http\Resources\UserCollection\UserCollection;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Users;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserApiController extends Controller
{

    public function index()
    {
        return UserCollection::collection(Users::all());

    }

    public function show($users)
    {
        $users = Users::find($users);
        // return $users;

       return new UserResourec($users);

    }
}
