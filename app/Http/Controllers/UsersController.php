<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Users;
use Validator;
use App\Http\Requests\StoreFormValidation;


class UsersController extends Controller
{
    public function index()
    {

        $users = Users::all();
        return view('users.index')->with(compact(['users']));
    }

    public function form()
    {
        return view('users.form');
    }

    public function edit(Users $User)
    {
        //  return($User);
        $ids = Users::find($User->id);
       if(empty($ids)){
            return abort(404);
       }
       return view ('users.edit')->with('Users', $ids);
    }

    public function destroy(){

    }

    public function store(StoreFormValidation $request) {


        $users = new users;
        $users->users = $request->users;
        $users->contact = $request->contact;
        $users->save();

        return redirect()->route('Users.index');


    }

    Public function update(Request $request, Users $User){

         //return($request);
            $users = Users::find($User->id);
            $users->users = $request->users;
            $users->contact = $request->contact;
            //return ($users);
            $users->save();

            // $users = Users::all();
            // // return $posts;
            // return view('users.index')->with(compact(['users']));
        return redirect()->route('Users.index');


    }

    public function show(Users $User){

        //return($User);
        $users = Users::find($User->id);
        $users->delete();

        // $users = Users::all();
        //     // return $posts;
        //     return view('users.index')->with(compact(['users']));

        return redirect()->route('Users.index');


    }

}
