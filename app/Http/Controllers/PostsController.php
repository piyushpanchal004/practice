<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Posts::all();
        // return $posts;
        return view('posts.index')->with(compact(['posts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $posts = new posts;
        $posts->posts = $request->posts;
        $posts->save();

        $posts = Posts::all();
        // return $posts;
        return view('posts.index')->with(compact(['posts']));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function show(Posts $Post)
    {

        return $Post;
        // echo "hello show";
        // exit();
        // $posts = Posts::all();
        // // return $posts;
        // return view('posts.index')->with(compact(['posts']));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function edit(Posts $Post)
    {

        $ids = Posts::find($Post->id);
       if(empty($ids)){
            return abort(404);
       }
        return view ('Posts.edit')->with('Post', $ids);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posts $Post)
    {

            $posts = Posts::find($Post->id);
            $posts->posts = $request->posts;
            $posts->save();



            $posts = Posts::all();
            // return $posts;
            return view('posts.index')->with(compact(['posts']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Posts $Posts)
    {
        $Posts = Posts::find($Posts);
        $Posts->delete();
    }
}
