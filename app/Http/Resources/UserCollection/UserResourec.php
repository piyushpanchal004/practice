<?php

namespace App\Http\Resources\UserCollection;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResourec extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return[
            'name' =>$this->users,
            'contact' =>$this->contact
        ];
        // return parent::toArray($request);
    }
}
