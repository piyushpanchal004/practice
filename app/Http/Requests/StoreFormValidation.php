<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFormValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'users'   => 'required|max:20',
            'contact' =>  'required|max:10',
        ];
    }

    public function messages()
    {
        return [
            'users.required'   => 'The name field is required.',
            'contact.max' =>  'Maximum 10',
        ];
    }
}
