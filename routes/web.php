<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::resource('Store', "StoreController");
// Route::view('Store','StoreController');

Route::resource('Categories', "CategoriesController");
Route::resource('Posts', "PostsController");

Route::resource('Users', "UsersController");
Route::get('users-form','UsersController@form')->name('Users.form');

// Route::get('users-form','UsersController@destory')->name('Users.form');
// Route::delete('/Users/{id}', UsersController@destroy);
// Route::get('users-edit','UsersController@edit')->name('Users.edit');
// Route::post('users-store','UsersController@store')->name('Users.store');

